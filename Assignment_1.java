import java.util.*;
/*
 * Remove duplicates from an array
 */
public class Assignment_1 {
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array[] = new int[size];
		for(int index=0 ; index<size ; index++){
			array[index] = scanner.nextInt();
		}
		Set<Integer> hash = new HashSet<Integer>();
		List<Integer> arr = new ArrayList<Integer>();
		for (int index=0 ; index<size ; index++){
			if(hash.isEmpty() || !hash.contains(array[index])) {
				arr.add(array[index]);
				hash.add(array[index]);
			}
		}
		int array2[] = new int[arr.size()];
		Iterator<Integer> itr = arr.iterator();
		for(int index=0; itr.hasNext(); index++){
			array2[index] = itr.next();
			System.out.print(array2[index]+" ");
		}
		scanner.close();
	}
}
